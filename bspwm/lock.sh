#!/bin/bash
icon="$HOME/Media/Pics/lock.png"
tmpbg='/tmp/screen.png'
(( $# )) && { icon=$1; }
import -window root $tmpbg
convert "$tmpbg" -scale 5% -scale 2000% "$tmpbg"
convert "$tmpbg" "$icon" -gravity center -composite -matte "$tmpbg"
set -e
xset s off dpms 0 10 0
omnipause pause
i3lock -u -i "$tmpbg"
xset s off -dpms
