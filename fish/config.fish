set PATH /usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/home/hs/.cargo/bin:/home/hs/.local/bin #$GOPATH/bin
set fish_greeting (date)

# aliases
alias rm='rm -I'
alias ls='/bin/ls --color=auto -CFX'
alias pg='/bin/ps aux | grep'
alias gvim='gvim -fn 6x10'
alias bc='/usr/bin/bc -ql'
alias eqb='equery b'
alias la='/bin/ls -alshX'
alias psall='/bin/ps aux'
alias psu='/bin/ps uxU $1'
alias psg='/bin/ps aux | grep $1'
alias lscrn='/usr/bin/screen -ls'
alias nscrn='/usr/bin/screen -S $1'
alias ascrn='/usr/bin/screen -D -R $1'
alias kill9='/bin/kill -9 $1'
alias df='/bin/df -kHl'
alias emerge='/usr/bin/emerge'
alias traceroute='traceroute -I'
alias cp='cp -p'
alias update="sudo dnf distro-sync && notify-send 'Updates Complete' 'Your system updated successfully!' -u normal -t 7500 -i checkbox-checked-symbolic -a Updates"
alias python3='python3.8'
alias python='python3'
alias colours="/home/hs/Dev/bash/colourpanes.sh"
alias get="sudo dnf install"

function get
        if sudo dnf install $argv
            notify-send "Install Successful!" "$1 installed successfully" -u normal -t 5000 -i checkbox-checked-symbolic;
        else
            notify-send "Install Failed" "$1 failed to install" -u critical -i error;
        end

    end

function fish_prompt
        set_color yellow

        if echo $PWD = "/home/hs" then
            set pwd "~"

        else
            set pwd $PWD
            echo hi
        end

        echo -e "\033[1A\033[K\033[1m<$PWD>"
        set_color green
        echo "»" (set_color normal)
    end

function c
        clear
    	fortune | cowsay -f tux | lolcat
    end
