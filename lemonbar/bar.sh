#!/bin/sh

while :; do
	volume=$(python volume.py)
	screen=$(python screen.py)
	window=$(xdotool getactivewindow getwindowname)
	date=$(date '+ %a %d %h %Y %k·%M·%S')
	battery=$(cat /sys/class/power_supply/BAT0/capacity | awk '{ print $1 "%"}')
	workspace=$(python workspace.py)

	sleep 0.1

	echo " $workspace  ⋮  $date   ⋮  ⚡  = $battery  ⋮  ♫ = $volume   ⋮  ☀ = $screen  ⋮  $window"

done
