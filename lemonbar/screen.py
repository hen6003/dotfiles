import subprocess

result = subprocess.run(["./screen.sh"], stdout=subprocess.PIPE) #runs command and gets output

end = str(result.stdout)[2:-6]

if len(end) == 1:
	end = "0" + end

if len(end) == 2:
	end = "0" + end

print(" " + end + "%")
