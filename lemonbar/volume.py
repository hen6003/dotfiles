import subprocess

result = subprocess.run(["/home/hs/.config/lemonbar/volume.sh"], stdout=subprocess.PIPE) #runs command and gets output
mute = subprocess.run(["/home/hs/.config/lemonbar/mute.sh"], stdout=subprocess.PIPE) #runs command and gets output

end = str(result.stdout)[32:-91]
end = end.strip()

muted = str(mute.stdout)[10:-3]
muted = muted.strip()

if end[-1:] == "/":
    end = end[:-1]

elif end == "":
    end = "00%"

elif end[-3:] == "/ -":
    end = end[:-4]

if end == "5% ":
    end = "05%"

end = end.strip()

if len(end) == 3:
    end = "0" + end

if muted == "yes":
    end = "───%"

if len(end) > 4:
    end = end[:4]

end = end.strip()

if len(end) == 3:
    end = "0" + end

print(end)
