import subprocess

result = subprocess.run(["/home/hs/.config/lemonbar/volume.sh"], stdout=subprocess.PIPE) #runs command and gets output
mute = subprocess.run(["/home/hs/.config/lemonbar/mute.sh"], stdout=subprocess.PIPE) #runs command and gets output

end = str(result.stdout)[32:-91]
end = end.strip()

muted = str(mute.stdout)[10:-3]
muted = muted.strip()

decimal = False
message = "["

if end[-1:] == "/":
    end = end[:-1]

elif end == "":
    end = "00%"

elif end[-3:] == "/ -":
    end = end[:-4]

if end == "5% ":
    end = "05%"

end = end.strip()

if len(end) == 3:
    end = "0" + end

end = end[:-1]

end = int(end) / 10

if str(end)[2:] == "5":
    decimal = True

end = int(end)

for i in range(end):
    message = message + "="

if decimal:
    message = message + "-"

while int(end) < 10:
    print("hi")
    message = message + " "
    end = end + 1

message = message + "]"

if muted == "yes":
    message = "[----------]"

subprocess.call("notify-send '" + message + "' -t 300 -a Volume", shell = True)
